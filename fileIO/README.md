this is a very simple in and out json handleing for a file

# how to use
import the modual by useing ` from fileIO import fileIO ` and instanceation the class with two arguments, with a directry name and the file name as so `fileClassExsample = fileIO("fileDir", "fileName.json")`, this setup will attempt to work with a file that is a directry called `\fileDir\` and a file called `fileName.json`, there are two methods

| method Name | aruments|
|---|---|
|getJsonFile| _filePath|
|WriteJsonFile| _content _filePath |
