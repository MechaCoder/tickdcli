from platform import system
from json import load
from json import dumps
from datetime import datetime

class fileIO():
    """
    very simple file input and output,
    [_fileDir - a directory that allready exists in the root of the progect]
    [_fileName - the name of the file you want to work pls include extenshion]
    """

    def __init__(self, _fileLocation="fileDir", _fileName="d.txt"):
       self.fileLocation = "{}/{}".format(_fileLocation, _fileName) # the directory of the file name
       pass

    def getJsonFile(self, _filePath=""):
        """ return a object from a file containing json """
        if _filePath == "":
            _filePath = self.fileLocation

        try:
            with open( "{}".format(self.fileLocation), mode='r', encoding='utf-8') as fileTemp:
                return load(fileTemp)
            pass
        except Exception as e:
            return {}
            print("[fileIO] there is a probem reading the file dose the really exist")


    def WriteJsonFile(self, _content={}, _filePath=""):
       """ returns True/False - takes object and filePathh writes object encodes to json and writes to file """
       if _filePath == "":
           _filePath = self.fileLocation
       if _content != "":
        # runs write code
           contentTemp = {
               "fileChange": str(datetime.now()),
               "content": _content
           }

           contentTempFile = dumps(contentTemp)
           with open( _filePath, mode='w', encoding='utf-8' ) as fileObject:
               fileObject.write(contentTempFile)
           return True

       else:
           print('[fileIO] pls add content to the function')
           return False
       pass
