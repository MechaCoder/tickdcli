from os import system

import click
from tickd import TickdCore
from fileIO import fileIO

tickdImp = TickdCore()
tickdFile = fileIO( "fileDir", "tickd.settings" )
@click.command()
# tickd implemention commands
@click.option('--add_tickd', default='', help='type in a tickd id to add it to tool')
@click.option('--read_tickd', default='', help='read all information that tool has on a tickd')
@click.option('--remove_tickd', default='', help='removes tickd from core')

# ticket command
@click.option('--read_ticket', default='', help='reads the ticket from the api based on its shortcode')
@click.option('--remove_ticket', default='', help='reasign Ticket')
@click.option('--list_tickets', default='', help='lists the tickets held localy')
@click.option('--copy_shortcode', default='', help='copys a shortcode to the clipboard' )

@click.option('--reasign_ticket', default='', help='reasigns a ticket to someone else')

def cli(add_tickd, read_tickd, remove_tickd, read_ticket, remove_ticket, list_tickets, copy_shortcode, reasign_ticket):
    """ powers the cli arguments and options """
    # loads tickd data from setting file

    system("cls")

    tickdFileTemp = tickdFile.getJsonFile() #gets the file content
    if len(tickdFileTemp) == 0:
        tickdFile.WriteJsonFile({"TickdArray": tickdImp.TickdArray, "tickets": tickdImp.TicketArray })
    tickdImp.TickdArray = tickdFileTemp['content']['TickdArray']
    tickdImp.TicketArray = tickdFileTemp['content']['tickets']

    if add_tickd != '': #add tickd,
        tickdInstanceExists = False
        for e in tickdImp.TickdArray:
            if e['id'] == add_tickd:
                tickdInstanceExists = True
            pass
        if tickdInstanceExists == False: #if the tickd instance dose not exist
            tickdImp.loadTickdInstance(add_tickd) # adds a instances to the tickdCore
            tickdFile.WriteJsonFile({"TickdArray": tickdImp.TickdArray, "tickets": tickdImp.TicketArray })
        else:
            click.echo("this tickd instance has allready been added")

    if read_tickd != '': #read tickd
        if read_tickd == 'all':
            tickdImp.tickdReadList()
        else:
            tickdImp.readTickdSingle(read_tickd, True)

    if remove_tickd != '': #remove_tickd
        tickdImp.removeTickdInstance(remove_tickd)
        tickdFile.WriteJsonFile({"TickdArray": tickdImp.TickdArray, "tickets": tickdImp.TicketArray })

    if read_ticket != '': #read a ticket
        tickdImp.getTicketbyShortCode(read_ticket)
        tickdFile.WriteJsonFile({"TickdArray": tickdImp.TickdArray, "tickets": tickdImp.TicketArray })

    if remove_ticket != '':
        tickdImp.removeTicketFromLocal(remove_ticket)
        tickdFile.WriteJsonFile({"TickdArray": tickdImp.TickdArray, "tickets": tickdImp.TicketArray })

    if list_tickets != '':
        if list_tickets.lower() == "all":
            tickdImp.readTicketList(True)
        else:
            tickdImp.listTicketByTickd( list_tickets )

    if copy_shortcode != '':
        tickdImp.shortCode2clipBoard( copy_shortcode )

    if reasign_ticket != '':
        tickdImp.reAsignTicket( reasign_ticket )

    print("[done]")
    pass
