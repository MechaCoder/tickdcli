# TickdCLI
---
tickd CLI is command tool witch works in the termanal layer to interact with
muiple impemetnions of tickd,

## commands
|command|argument| notes|
|---|---|---|
|--add_tickd| tickd id| this adds a imperment of tickd to the tool, the argument is a user defined id andis used to insure the tickd id dose not allready exist.
|--read_tickd | tickd id | this reads the information on a implement of tickd by its id
|--remove_tickd | tickd id | this removes a tickd instance from the tool|
|--read_ticket | shortCode | reads a ticket from a aproprate api and then caches the tickets localy


# project documentation
## project plumming
```
tickdCLI.py
 |- tickd - interactation with the tickd api
 |- fileIO - keeping tool state
```
## Words we say

| Term | Def | History
|---|---|---
|tickd id | this is the user defined tickd id whitch is used to genorate the shortCode | |
|ticket number \ ticket id | this is the id of the ticket in a tickd instance| tickd Genrates this
|tickd instance| a instance of tickd is tickd witch has a eunque URL, | this is needed becouse of the need to be able to works with muliple ticks each have a requirement of a url and authorization key.
|shortCode \ short code | this is a term used to discribe a unquie ticket based on to poices of information the first is the tickd id and the secound the ticket number.| i started useing short codes for tickets becouse of a need to keep track of tickets from diffrent tickds.


## Words we don't say
| Term | why not | use insted
| --- | --- | --- |
|tickd user | this is a single user system | user|
