from setuptools import setup

setup(
    name="tickdCli",
    version="0.1 alfa",
    py_modules=["tickdCli"],
    install_requires=['Click'],
    entry_points='''
        [console_scripts]
        tickdCli=tickdCli:cli
    '''
)
