import datetime
import os
# from tkinter import Tk
from tabulate import tabulate as table

def dictKeyExists(_struct={}, _key=""):
    try:
        t = _struct[_key]
        return True
    except:
        return False
    pass

def strip_html(html):
    html1 = html[::-1]
    html2 = html1.split(">")
    html3 = html2[0][::-1]
    return html3

def valdateTickdInstance(_dict={}):
    if dictKeyExists(_dict, "id") and dictKeyExists(_dict, "url") and dictKeyExists(_dict, "key"):
        return True
    return False

def filterTicketData(_data={}, _sortCode="", _ticketId=""):
    if _data != {}:
        # try:
            if _data['statuscode'] == 200 and _data != False:
                rData = {
                    "shortCode" : _sortCode,
                    "statuscode" : _data["statuscode"],
                    "localTicket": False,
                    "ticket": {
                        "title": _data["content"]["title"],
                        "deisc": strip_html(_data["content"]["description"]),
                        "createdBy": {
                            "name": _data["content"]["createdby"]["name"],
                            "email": _data["content"]["createdby"]["email"]
                        },
                        "ticketChanges": [],
                        "notesOnTicket": [],
                        "assignmentHistory": [],
                        "svnCommit":{"count":0, "items":[]}
                    },
                    "rawData": _data
                }
                if str( type( _data['content']['assigned'] ) ) != "<class 'NoneType'>":
                    temp = {'id': _data['content']['assigned']['id'], 'name': _data['content']['assigned']['name'] }
                    rData["assignedTo"] = temp
                else:
                    rData["assignedTo"] = {}

                for e in _data["content"]["notes"]["items"]:
                    if e["type"]["label"] == "ticket change":
                        if 'assigned this ticket to' in e["body"]: # when the ticket is reasgined
                            rData["ticket"]["assignmentHistory"].append(
                                {
                                "assigned": e["createddate"],
                                "assigned by": e["body"].split(" ")[0],
                                "assigned by id": e["createdby"]["id"],
                                "assigned to": e["body"].split(" ")[-1]
                                }
                            )
                        else:
                            rData["ticket"]["ticketChanges"].append(e)
                    elif e["type"]["label"] == "SVN checkin":
                        rData["ticket"]["svnCommit"]["count"] += 1
                        if e["createdby"] != None:
                            rData["ticket"]["svnCommit"]["items"].append(
                                {
                                    "commitBy": e["createdby"]["name"],
                                    "on": e["createddate"],
                                }
                            )
                        pass
                    else:
                        e["body"] = strip_html( e["body"] )
                        rData["ticket"]["notesOnTicket"].append(e)
            elif _data['statuscode'] != 200:
                if _data['statuscode'] == 500:
                    print("oops some one put some 50 pence in the meter")
                rData = EmptyTicketDict( _sortCode )
            else:
                rData = EmptyTicketDict(_sortCode)
            return rData
        # except Exception as e:
        #     print("conection has failed pls enter")
        #     print("Details; [{}]".format(e))
        #     return EmptyTicketDict( _sortCode )

def EmptyTicketDict(_sortCode):
    rData = {
        "shortCode": _sortCode,
        "statuscode": "0",
        "ticket":{
            "title": str(input(" give ticket a title ")),
            "deisc": str(input(" give the ticket a discription ")),
            "createdby": {
            "name": "",
            "email": ""
            }
        },
        "ticketChanges": [],
        "notesOnTicket": [],
        "assignmentHistory": [],
        "svnCommit":{"count":0, "items":[]}
    }
    return rData

def findArrayLocation(_array=[], _key="", _value="" ):
    tempInt = 0
    for e in _array:
        if e[_key] == _value:
            return tempInt
        tempInt = tempInt + 1
    return False

def breakDownShortcode(_shortCode=""):
    if _shortCode != "":
        t = _shortCode.split(" - ")
        return t
    else:
        return False

def convertDate(_int="0", _dateString="%d/%m/%y %H:%M"):
    temp = str(datetime.datetime.fromtimestamp( int( _int ) ).strftime(_dateString))
    return temp

def readTicketSingle(_data={}):
    if _data != {}:
        if dictKeyExists(_data['ticket'], 'title'):
            print("Ticket Title : {}".format(_data['ticket']['title']))


        if dictKeyExists(_data['ticket'], 'deisc'):
            print("Ticket Discription : {}".format( shortenString(
                _data['ticket']['deisc'])
            ))


        if dictKeyExists(_data['ticket'], 'createdby'):
            if dictKeyExists(_data['ticket']['createdby'], 'name'):
                print("created by: {}".format(_data['ticket']['createdby']['name']))

        if dictKeyExists(_data['ticket'], 'notesOnTicket'):
            print("Notes On Ticket")
            _data['ticket']['notesOnTicket'].reverse() #reverse is done in place
            if len(_data['ticket']['notesOnTicket']) != 0:
                for e in _data['ticket']['notesOnTicket']:
                    if len(e['createdby']['name']) == 0:
                        e['createdby']['name'] = "someone"
                    print(">> {} wrote; \n {} \n >> on the  {}".format(
                        e['createdby']['name'],
                        e['body'],
                        convertDate(int(e['createddate']), "%d/%m") )
                    )
                    print("______________________________________________________________________________")


        if dictKeyExists(_data['ticket'],'assignmentHistory'):
            print("The Assignment History")
            for e in _data['ticket']['assignmentHistory']:
                dateVal = convertDate(e['assigned'], "%d/%m")
                assignedTo = e['assigned to']
                print(">> {} - ticket was assigned to {}".format( dateVal, assignedTo ))
            print("")

        if dictKeyExists(_data['ticket'], 'svnCommit'):
            print("there have been {} commits.".format(_data['ticket']['svnCommit']['count']))
        return True

def shortenString(_string="", _amount=140):
    if len(_string) > _amount:
        return _string[:_amount]
    else:
        return _string

def yesNo(_qwestion="what do want to do?"):
    inputVal = str(input(_qwestion))
    while True:
        if inputVal.lower() == "yes" or inputVal.lower() == "y":
            return True
        elif inputVal.lower() == "no" or inputVal.lower() == "n":
            return False
        pass

def openTicketinCrome(_ticketURL="", _ticketId="",):
    commandString = "start chrome {}/ticket/?/{}".format(_ticketURL, _ticketId)
    os.system( commandString )

def stringtoclipboard( _string="" ):
    print( _string )
    commandString = "echo {} | clip".format( _string.strip() )
    os.system( commandString )

def renderTable(_data=[["foo", "bar"]], _headers=["id", "value"], _tableType="psql" ):
    t = table( _data, _headers, _tableType )
    print( t )

def getPeopleFromTicket(_data={}):
    people = {} # people = {name: id }
    _data = _data['content']

    if dictKeyExists( _data, "createdby" ):
        people[_data['createdby']['name']] = _data["createdby"]['id']
    #
    if dictKeyExists( _data, "assigned" ):
        if _data["assigned"] != None:
            people[_data['assigned']['name']] = _data["assigned"]['id']

    # #
    if dictKeyExists( _data, "lastactive"):
        people[_data['lastactive']['name']] = _data['lastactive']['id']
    #
    if dictKeyExists( _data, "notes" ):
        for note in _data['notes']['items']:
            if note != None and note['createdby'] != None:
                people[note['createdby']['name']] = note['createdby']['id']
    print( people )
    return people
