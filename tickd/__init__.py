import tickd.tickdAPI
import tickd.tickdUtils

class TickdCore():
    """ tickd core is a stand alone class for dealing with the tickd API """

    def __init__(self):
        self.TicketArray = []
        self.TickdArray = []
    # meathos for tickd

    def __str__(self):
        return "you are running {} instances, and have {} tickets saved".format(
            self.TickdArray,
            self.TicketArray
        )

    def loadTickdInstance(self, _id="", _url="", _key=""):
        """ loads a tickd instance into useable instances """
        if len(_id) != 0 and len(_url) != 0 and len(_key) != 0:
            self.TickdArray.append( {"id": _id, "url": _url, "key": _key})
            return True
        else:
            if len(_id) == 0:
                _id = str(input("pls enter the local id of the ticked > "))
            if len(_url) == 0:
                _url = str(input("pls enter the base URL > "))
            if len(_key) == 0:
                _key = str(input("pls enter the api Acesses key > "))
            self.loadTickdInstance(_id, _url, _key)

    def readTickdInstance(self, _print=False):
        """ reads out the tickd objects if _print is true then tickd objects inline if False [default] returns the tickd array """
        if str(_print) == str(True):
            for t in self.TickdArray:
                print("{} - {}".format(t["id"], t["url"]) )
            return True
        else:
            return self.TickdArray

    def readTickdSingle(self, _id="", _print=False):
        if _id != "":
            for e in self.TickdArray:
                if _id == e['id']:
                    if _print:
                        print("the tickd ID : {}".format(e['id']))
                        print("the tickd base URL: {}".format(e['url']))
                        print("the tickd authorization key: {}".format(e['key']))
                        return True
                    else:
                        return e
            pass
            if _print:
                print("tickd Instance {} not found".format(_id))
            else:
                return "tickd instance {} not found".format(_id)
        else:
            if _id == "":
                _id = str(input("enter an id of a tickd instance: "))

    def removeTickdInstance(self, _id=""):
        """ removes a tickd instance by it's id """
        if len(_id) != 0:
            self.readTickdInstance(True)
            res = tickdUtils.findArrayLocation(self.TickdArray, "id", _id)
            if str(res) != str(False):
                self.TickdArray.pop(int(res))
                return True
            else:
                self.removeTickdInstance()
        else:
            print("enter the id of the tickd instance you want remove")
            tId = str(input("tickd id > "))
            self.removeTickdInstance(tId)

    # tickets
    def getTicketfromAPI(self, _tickdid="", _ticketId=""):
        """ gets the ticket form the api and filters the object """
        if len(_tickdid) != 0 and len(_ticketId) != 0:
            tickdData = {}
            for e in self.TickdArray:
                if e["id"] == _tickdid:
                    tickdData = e
            if tickd.tickdUtils.valdateTickdInstance(tickdData):
                data = tickd.tickdAPI.getTicket( tickdData["url"], tickdData["key"], _ticketId )
                if data['success'] == True:
                    data['info'] = tickd.tickdUtils.filterTicketData(data['info'], "{} - {}".format(_tickdid,_ticketId))
                return data
            else:
                self.getTicketfromAPI()
        else:
            if len(_tickdid) == 0:
                self.readTickdInstance()
                print("enter the id of the tickd you want to use")
                _tickdid = str(input(" > "))

            if len(_ticketId) == 0:
                print("enter ticket number")
                _ticketId = str(input(" > "))
            self.getTicketfromAPI(_tickdid, _ticketId)
        pass

    def addTicketToLocal(self, _ticketObject={}):
        """ add a ticket to the ticket object """
        if _ticketObject != {}:
            self.TicketArray.append(_ticketObject)
            return True
        else:
            return False
        pass

    def readTicketList(self, _print=False):
        """ read all tickets that are saved in this instance """
        if _print:
            tableData = []
            for e in self.TicketArray:
                # print(e)
                # print("{} - {}".format(e["shortCode"], e["ticket"]["title"]))
                tableData.append([e["shortCode"], e["ticket"]["title"]])
            tickd.tickdUtils.renderTable( tableData, ['ticket shortCode', "Title"] )
            return True
        else:
            return self.TicketArray

    def removeTicketFromLocal(self, _shortCode=""):
        """ removes the ticket by it shortCode """
        if _shortCode != "":
            res = tickd.tickdUtils.findArrayLocation(self.TicketArray, "shortCode", _shortCode)
            if type(res) == type(0):
                self.TicketArray.pop(res)
                return True
            else:
                self.removeTicketFromLocal()
        else:
            print("enter the id of the tickd instance you want remove")
            tId = str(input("tickd id > "))
            self.removeTicketFromLocal(tId)

    def getTicketbyShortCode(self, _shortcode="", _print=True):
        """ intellgently find a ticket weather it is saved localay or though api and outputs"""
        if _shortcode != "":
            # shearch though the local for short code.
            ticketShortCode = tickd.tickdUtils.breakDownShortcode(_shortcode)
            ticket = self.getTicketfromAPI(ticketShortCode[0], ticketShortCode[1])
            if ticket['success'] == True:
                added = False
                for ticketLocal in self.TicketArray:
                    if ticketLocal["shortCode"] == ticket['info']['shortCode']: # is the ticket allready in the local array
                         self.removeTicketFromLocal( ticket['info']['shortCode'] )
                self.addTicketToLocal(ticket['info'])
                if _print == True:
                    tickd.tickdUtils.readTicketSingle(ticket['info'])
                else:
                    return ticket['info']
                if tickd.tickdUtils.yesNo("would you like to see this ticket in chrome : "): #returns a boolen
                    tlocal = tickd.tickdUtils.findArrayLocation(self.TickdArray, "id", ticketShortCode[0])
                    tickd.tickdUtils.openTicketinCrome(self.TickdArray[tlocal]['url'], ticketShortCode[1])
            else:
                for e in self.TicketArray:
                    if e['shortCode'] == _shortcode:
                        if _print == True:
                            tickd.tickdUtils.readTicketSingle(e)

                        if ticket['success'] == True:
                            if tickd.tickdUtils.yesNo("would you like to see this ticket in chrome : "): #returns a boolen
                                tlocal = tickd.tickdUtils.findArrayLocation(self.TickdArray, "id", t[0])
                                tickd.tickdUtils.openTicketinCrome(self.TickdArray[tlocal]['url'], t[1])

                        return e
        else:
            _shortcode = str(input("enter a shortCode"))
            self.getTicketbyShortCode(_shortcode)
        pass


    def listTicketByTickd(self, _ticketId=""):
        """ prints out a list of tickets based on there connection to projects """
        if _ticketId != "":
            tableData = []
            for ticket in self.TicketArray:
                if tickd.tickdUtils.breakDownShortcode(ticket['shortCode'])[0] == _ticketId:
                    # print("{} - {}".format( ticket['shortCode'], ticket['ticket']['title'] ))
                    tableData.append( [ticket['shortCode'], ticket['ticket']['title'] ] )
            tickd.tickdUtils.renderTable( tableData, ['ticket shortCode', "Title"] )

        else:
            _ticketId = str(input( "enter the tickd id of the tickd you want to read" ))

    def reAsignTicket(self, _shortcode=""):
        """ reasigns a ticket to a user based on a list of people who assigned the ticket to someone else """
        if _shortcode != "":
            ticketShortCode = tickd.tickdUtils.breakDownShortcode( _shortcode )
            ticket = self.getTicketbyShortCode( _shortcode, False )
            tickdInstance = {}

            for t in self.TickdArray:
                if ticketShortCode[0] == t['id']:
                    tickdInstance = t

            people = tickd.tickdUtils.getPeopleFromTicket( ticket['rawData'] )

            listPeople = list( people.keys() )
            peopleNameLast = listPeople[-1]
            listPeople.remove( listPeople[-1] )
            peopleNamesStr = ""

            for person in listPeople:
                peopleNamesStr = peopleNamesStr + "{}, ".format(person)

            peopleNamesStr = peopleNamesStr + "and {}".format( peopleNameLast )

            print( "people who you can be assigned this ticket; {}".format( peopleNamesStr ))
            inputStr = str(input( "enter the name of the person you want to assign this ticket too" ))
            if inputStr in listPeople:
                tickd.tickdAPI.putTicketAssigned( tickdInstance['url'], tickd['key'], ticketShortCode[1], people[inputStr] )

            # ticketNumber
            # person id
            pass
        else:
            _shortcode = str(input("short code of the ticket that you want to reasign: "))


    def shortCode2clipBoard(self, _shortcode=""):
        """ copyes a inputed shortcode to clipboard """
        tickd.tickdUtils.stringtoclipboard(_shortcode)

    def tickdReadList(self):
        tickdFillter = []
        for readTickdInstanceTemp in self.TickdArray:
            tickdFillter.append( [readTickdInstanceTemp["id"], readTickdInstanceTemp["url"]] )
        tickd.tickdUtils.renderTable(tickdFillter, ["id", "url"])
