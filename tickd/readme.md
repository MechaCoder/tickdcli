# Tickd module
this python has been written to help people who want to work with one or more
tickd instance, using this package enables you to readTickets, and reasign
tickets to new users.
### >Important the package is only tested on Python 3.5 and virtualenv is not being used

## package layout
```
ROOT
 |- __init__.py
 |- tickdAPI.py
 |- tickdUtils.py
```

# How to use
This package is self-contained and can be dropped into a project, once this
Is done. You need to instantiate the TickdCore class by now you can use the class
to interact with tickd, think of the class as the internal Python API for doing
some things but not everything.

## Getting started
you will need to drag and drop the directory into your project and import the
package by typing ` import tickd ` but if want to be consistent with pep 20 and 8
I would  `from tickd import TickdCore.'

## unique concepts
### shortCode;
A short Code as a notation for a ticket id witch describes both the where the
ticket is and the ticket id so an example would be `tom - 1234` this example can
be broken down into tickdInstance called tom and the ticket which has the id 1234

---

### class
#### methods
##### loadTickdInstance
this is a method for adding an instance of tickd, one you have added a tickd instance,
###### arguments
name | type | notes | default
--- | --- | --- | ---
 id | string | This is the id that you will use when shearch by shortCode | `""`
 url| string | This is a the base of your URL of the tickd intance you can get by placeing `window.location.hostname` into a chrome or firefox devTools. |`""`
 key | string | This is authorization key for the API, you can find this by looking in your user setting and in thhe section marked as API | `""`
***
##### readTickdInstance
This will ether return all tickd instance or will print
###### arguments
name | type | notes | default
--- | --- | --- | ---
print | boolen | this will allow you too print out to the window [False] or return all tickd instances as a list of dicts | False
##### removeTickdInstance
removes a instance of tickd from being used
###### arguments
name | type | notes | default
--- | --- | --- | ---
id | string | the id of the tickd instance. | ""
***
##### getTicketfromAPI
this will go and get the tickd and get the ticket form an api based on the id of the tickd instance
###### arguments
name | type | notes | default
--- | --- | --- | ---
tickdId | string | the tickd id of the tickd you want use | `""`
ticket | string | the ticket number | `""`
##### removeTickdInstance
removes a instance of tickd from being used
###### arguments
name | type | notes | default
--- | --- | --- | ---
id | string | the id of the tickd instance. | `""`
ticketid | string | the ticket id | `""`
##### getTicketbyShortCode
gets the Ticket by its ShortCode.
###### arguments
name | type | notes | default
--- | --- | --- | ---
shortCode | string | the shortCode of the ticket you want | `""`
##### list_tickets
lists the tickets held localy
##### arguments
name | type | notes | default
--- | --- | --- | ---
tickdId | string | the id of a tickd held by the system filter the ticket if `all` is entered then all tickets are printed  | `""`


# project stuff
## conventions
### Indentation
in the project, all indentaion is made of increments and decrements of 4 spaces
### commenting
each class method, function in __init__.py must have a docString
### code line length
code should be no more than 80 char except when this would break the zen of python,
the most often case is the long conditional statement and when moving through a large
object

in the tickdAPI.py file all functions should always return a dict with a mimmum of two keys the first is the success this signifis weather a request as sucssed or not and must be a boolen this can tested for the instance the secound is the returned data or in the event a black directory.
