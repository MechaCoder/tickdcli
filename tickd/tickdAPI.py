import http.client
import json
import sys

def getTicket(_tickdUrl="", _key="",_ticketId=""):
    """ connects to the tickd api and GETS data """
    conn = http.client.HTTPSConnection( _tickdUrl )

    headers = {
        'content-type': "Application/json",
        'authorization': _key,
        'cache-control': "no-cache"
        }
    try:
        conn.request("GET", "/api/v1/index.cfm/ticket/{}".format(_ticketId), headers=headers)

        res = conn.getresponse()
        data = res.read()
        conn.close()

        # print( data.decode("utf-8") )
        struct = json.loads( data.decode("utf-8") )

        return {"success": True, "info": struct}
    except:
        return {'success': False, "info":sys.exc_info() }



def putTicketAssigned( _tickdUrl="", _key="", _ticketId="", userId=0):
    conn = http.client.HTTPConnection(_tickdUrl)

    payload = json.dumps({"assigned": userId})
    headers = {
        'content-type': "Application/json",
        'authorization': _key
        }

    conn.request("PUT", "/api/v1/index.cfm/ticket/{}".format(_ticketId), payload, headers)

    res = conn.getresponse()
    data = res.read()
    return json.loads( data.decode("utf-8") )
